#!/usr/bin/env python

import random

class Character:
    def __init__(self, name, character_type, max_hp, max_mp, ap, actions):
        self.name = name
        self.character_type = character_type
        self.max_hp = max_hp
        self.level = 1
        self.hp = max_hp
        self.max_mp = max_mp
        self.mp = max_mp
        self.ap = ap
        self.actions = actions
   
    # Providing info about the Character
    def show_stats(self):
        print('HP: %4d/%4d, MP: %4d/%d' % (self.hp, self.max_hp, self.mp, self.max_mp))
       
   
    def rest(self):
        self.hp = self.max_hp
        self.mp = self.max_mp
        print('Your points have been restored to the maximum value!')
       
       
    def is_dead(self):
        return self.hp <= 0
   
    def get_actions(self):
        action_list = []
        for action in self.actions:
            action_list.append(action)
        return action_list
   
    @staticmethod
    def get_character_list():
        char_list = [subclass.__name__ for subclass in Character.__subclasses__()]
        return char_list
   
    @staticmethod
    def init_given_character(character_name):
        for subclass in Character.__subclasses__():
            if character_name == subclass.__name__:
                return subclass()
           
    @staticmethod
    def init_random_character():
        character_list = Character.__subclasses__()
        character_index = random.randrange(0, len(character_list))
        return character_list[character_index](False)
   
   
class Warrior(Character):
    def __init__(self,is_player=True):
        if(is_player):
            character_type= 'Player'
        else:
            character_type= 'NPC'
        max_hp=1000
        max_mp=10
        ap=20
        actions = [{'name': 'Punch', 'type': 'physical', 'dmg': 50, 'succ': 0.90,'mp_cost':0},
                   {'name': 'Kick', 'type': 'physical', 'dmg': 60, 'succ': 0.80,'mp_cost':0},
                   {'name': 'Chuck Norris roundhouse kick', 'type': 'physical', 'dmg': 9999, 'succ': 0.001, 'mp_cost':0}]
        super().__init__(type(self).__name__,character_type,max_hp,max_mp,ap,actions)
               
           
class Wizzard(Character):
    def __init__(self,is_player=True):
        if(is_player):
            character_type= 'Player'
        else:
            character_type= 'NPC'
        max_hp=1000
        max_mp=100
        ap=15
        actions = [{'name': 'Punch', 'type': 'physical', 'dmg': 50, 'succ': 0.90, 'mp_cost':0},
                   {'name': 'Kick', 'type': 'physical', 'dmg': 60, 'succ': 0.80,'mp_cost':0},
                   {'name': 'Fire', 'type': 'spell', 'dmg': 60, 'succ': 0.80, 'mp_cost':0}
                    ]
        super().__init__(type(self).__name__,character_type,max_hp,max_mp,ap,actions)

class CustomCharacter(Character):
    def __init__ (self, attributes):
        character_type = "NPC"
        max_hp = attributes[0]
        max_mp = attributes[1]
        ap = attributes[2]
        actions = attributes[3]

        super().__init__(type(self).__name__, character_type,max_hp,max_mp,ap,actions)