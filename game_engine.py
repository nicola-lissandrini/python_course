#!/usr/bin/env python
import random
from characters import *
import numpy as np

class Fight:
    def __init__ (self, user, ai):
        self.user = user
        self.ai = ai
        self.turns = 0

    def check_dead (self):
        if (self.user.is_dead ()):
            return 1
        elif (self.ai.is_dead ()):
            return 2
        return 0

    def chooseRandomAction (self, character):
        while (True):
            choice = random.randrange (0, len (character.actions))
            
            if (character.actions[choice]["mp_cost"] < character.mp):
                print ("Enemy chooses %s" % character.actions[choice]["name"])
                return choice

    def chooseUserAction (self, character):
        while (True):
            print ("Choose an action (-1 to flee)")
            i = 0
            for action in character.actions:
                print ("%d: %s" % (i, action["name"]))
                i += 1
            actionNo = int (input ())

            if (actionNo < 0):
                return actionNo

            if (character.actions[actionNo]["mp_cost"] < character.mp):
                return actionNo
            else:
                print ("You don't have enough MP")
        
    
    def doAttack (self, attacker, defendant, action):
        cost =  attacker.actions[action]["mp_cost"]
        successProbability = attacker.actions[action]["succ"]
        dmg = attacker.actions[action]["dmg"]

        attacker.mp -= cost

        if (random.random () < successProbability):
            effectiveDamage = dmg * (attacker.ap + random.randrange (-5, 5))
            defendant.hp -= effectiveDamage
            print ("Hit")
        else:
            print ("Miss")

    
    def attackLoop (self):
        turn = 0
        self.turns = 0

        print ("Wild %s appears" % self.ai.name)

        while (self.check_dead () == 0):
            self.turns += 1
            if (turn == 0):
                print ("Its your turn")
                attacker = self.user
                defender = self.ai

                action = self.chooseUserAction (attacker)
            else:
                print ("Its enemy's turn")
                attacker = self.ai
                defender = self.user

                action = self.chooseRandomAction (attacker)

            turn = (turn + 1) % 2


            if (action < 0):
                if (random.random () < 0.5):
                    print ("You fled")
                    return 0
                else:
                    print ("You cannot flee")
            else:
                self.doAttack (attacker, defender, action)
                print ("Your HP: %d, your MP %d \n Enemy HP: %d" % (self.user.hp, self.user.mp, self.ai.hp))

        return self.check_dead ()
class GameEngine:
    def __init__(self):
        self.states = ["start", "home", "quit", "explore", "victory"]
        self.currentState = 0
        self.places = ["cave","castle","skyscraper","airport"]
        self.currentPlace = 0
        self.player = []
        self.enemyProbability = 0.5

    def setup (self):
        print ("Choose a character:")
        print (Character.get_character_list ())
        player_name = input ()
        self.player = Character.init_given_character (player_name)
        self.player.show_stats ()
        return 1
    
    def chooseHomeAction (self):
        self.player.rest ()

        print ("Continue exploring? [y/n]")
        resp = input ()
        if (resp == "y"):
            return 3
        else:
            return 2

    def fight (self):
        enemy = Character.init_random_character ()

        battle = Fight (self.player, enemy)

        result = battle.attackLoop ()
        if (result == 1):
            return 1
        elif (result == 2):
            self.player.level += 1
        
        return 3

    def explore (self):
        self.currentPlace = int (random.randrange (0, len (self.places)))
        print ("You are in %s" % self.places [self.currentPlace])

        if (random.random () < self.enemyProbability):
            return self.fight ()
        else:
            print ("Continue exploring or go home and rest? [e/h]")
            resp = input ()
            if (resp == "h"):
                return 1
            else:
                return 3
               

    def action (self):
        # start
        if (self.currentState == 0):
            self.currentState = self.setup ()
        # home
        elif (self.currentState == 1):
            self.currentState = self.chooseHomeAction ()
        # quit
        elif (self.currentState == 2):
            self.currentState = 0
        # explore
        elif (self.currentState == 3):
            self.currentState = self.explore ()

def play ():         
    engine = GameEngine ()

    while True:
        engine.action ()