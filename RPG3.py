#!/usr/bin/env python
# coding: utf-8

# In[2]:


import random
import numpy as np
class Character:
    def __init__(self, name, character_type, max_hp, max_mp, ap, actions,increase_hp = 50,increase_mp =5,increase_ap=3): 
        self.max_hp = max_hp
        self.hp = max_hp
        self.name = name
        self.character_type = character_type
        self.max_mp = max_mp
        self.mp = max_mp
        self.ap = ap
        self.player_level = 0
        self.exp = 0
        self.actions = actions
        self.increase_hp = increase_hp
        self.increase_mp = increase_mp
        self.increase_ap = increase_ap
        self.loose = 0
        if character_type == 'Player':
            self.actions.append({'name': 'Heal', 'type': 'N/a', 'dmg': 0, 'succ': 0.9, 'mp_cost': 0})
            self.actions.append({'name': 'Try to escape', 'type': 'N/a', 'dmg': 0, 'succ': 0.5, 'mp_cost': 0})
    
    #Providing info about the character
    def show_stats(self):
        print('HP:%4d/%d, MP:%4d/%d' % (self.hp, self.max_hp, self.mp, self.max_mp))

    def rest(self):
        self.hp = self.max_hp
        self.mp = self.max_mp
        self.loose = 0
        
    def is_dead(self):
        return self.hp <= 0
    
    def increase_Level(self):
        self.max_hp += self.increase_hp
        self.max_mp += self.increase_mp
        self.ap += self.increase_ap
        self.rest()
    
    def get_actions(self):
        action_list = []
        for action in self.actions:
            action_list.append(action)
        return action_list
    
    @staticmethod
    def get_character_list():
        char_list = [subclass.__name__ for subclass in Character.__subclasses__()]
        return char_list
    
    @staticmethod
    def init_given_character(character_name):
        for subclass in Character.__subclasses__():
            if character_name == subclass.__name__.lower():
                return subclass()
            
    @staticmethod
    def init_random_character():
        character_list = Character.__subclasses__()
        char_index = random.randrange(0,len(character_list))
        return character_list[char_index](False)


class Warrior(Character):
    def __init__(self, is_Player=True):
        if(is_Player):
            character_type = 'Player'
        else:
            character_type = 'NPC'
        max_hp = 1500
        max_mp = 20
        ap = 15
        actions = [{'name': 'Punch', 'type': 'physical', 'dmg': 50, 'succ': 0.90,'mp_cost': 0},
                   {'name': 'Kick', 'type': 'physical', 'dmg': 60, 'succ': 0.80, 'mp_cost': 0},
                   {'name': 'Sword cleave', 'type': 'physical', 'dmg': 80, 'succ': 0.80, 'mp_cost': 0},
                   {'name': 'Magic sword cleave', 'type': 'physical', 'dmg': 100, 'succ': 0.55, 'mp_cost': 10}]
        super().__init__(type(self).__name__, character_type, max_hp, max_mp, ap, actions,100,3,2)
        
    def get_type(self):
        return 'Warrior'
        
class ArmoredWarrior(Character):
    def __init__(self, is_Player=True):
        if(is_Player):
            character_type = 'Player'
        else:
            character_type = 'NPC'
        max_hp = 2000
        max_mp = 20
        ap = 12
        actions = [{'name': 'Punch', 'type': 'physical', 'dmg': 50, 'succ': 0.90,'mp_cost': 0},
                   {'name': 'Kick', 'type': 'physical', 'dmg': 60, 'succ': 0.80, 'mp_cost': 0},
                   {'name': 'Shield Hit', 'type': 'physical', 'dmg': 70, 'succ': 0.80, 'mp_cost': 0},
                   {'name': 'Rolled Shield', 'type': 'physical', 'dmg': 90, 'succ': 0.7, 'mp_cost': 0}]
        super().__init__(type(self).__name__, character_type, max_hp, max_mp, ap, actions,200,2,1)
        
    def get_type(self):
        return 'ArmoredWarrior'
        
class Wizard(Character):
    def __init__(self, is_Player=True):
        if(is_Player):
            character_type = 'Player'
        else:
            character_type = 'NPC'
        max_hp = 500
        max_mp = 100
        ap = 50
        actions = [{'name': 'Punch', 'type': 'physical', 'dmg': 50, 'succ': 0.90, 'mp_cost': 0},
                   {'name': 'Kick', 'type': 'physical', 'dmg': 60, 'succ': 0.80, 'mp_cost': 0},
                   {'name': 'Fire', 'type': 'spell', 'dmg': 75, 'succ': 0.80, 'mp_cost': 10},
                   {'name': 'Thunder', 'type': 'spell', 'dmg': 110, 'succ': 0.5, 'mp_cost': 20},
                   {'name': 'Blizzard', 'type': 'spell', 'dmg': 85, 'succ': 0.65, 'mp_cost': 10}]
        super().__init__(type(self).__name__, character_type, max_hp, max_mp, ap, actions,25,10,10)

    def get_type(self):
        return 'Wizard'        
        
        
class Rogue(Character): 
    def __init__(self, is_Player=True):
        if(is_Player):
            character_type = 'Player'
        else:
            character_type = 'NPC'
        max_hp = 750
        max_mp = 50
        ap = 35
        actions = [{'name': 'Punch', 'type': 'physical', 'dmg': 50, 'succ': 0.90, 'mp_cost': 0},
                   {'name': 'Kick', 'type': 'physical', 'dmg': 60, 'succ': 0.80, 'mp_cost': 0},
                   {'name': 'Shiv', 'type': 'physical', 'dmg': 80, 'succ': 0.80, 'mp_cost': 0},
                   {'name': 'Magic Shiv', 'type': 'spell', 'dmg': 100, 'succ': 0.75, 'mp_cost': 10}]
        super().__init__(type(self).__name__, character_type, max_hp, max_mp, ap, actions,50,5,4)       

    def get_type(self):
        return 'Rogue'        
        
        
class Ranger(Character): 
    def __init__(self, is_Player=True):
        if(is_Player):
            character_type = 'Player'
        else:
            character_type = 'NPC'
        max_hp = 1000
        max_mp = 5
        ap = 25
        actions = [{'name': 'Punch', 'type': 'physical', 'dmg': 50, 'succ': 0.90,'mp_cost': 0},
                   {'name': 'Kick', 'type': 'physical', 'dmg': 60, 'succ': 0.80, 'mp_cost': 0},
                   {'name': 'Skyuppercut', 'type': 'physical', 'dmg': 90, 'succ': 0.75, 'mp_cost': 0},
                   {'name': 'Chuck Norris roundhouse kick', 'type': 'physical', 'dmg': 9999, 'succ': 0.01, 'mp_cost': 0}]
        super().__init__(type(self).__name__, character_type, max_hp, max_mp, ap, actions,75,4,3)

        
    def get_type(self):
        return 'Ranger'


# In[29]:


import pandas as pd
class GameEngine:
    def __init__(self):
        self.states = ['quit','explore','rest and back to home']
        self.places = ['home', 'garden', 'path', 'city', 'river', 'castle', 'dragon house', 'victory road']
        self.current_state = 0
        self.current_place = 0
        self.playing = 0
    
    #initialization of the game, choice of level and of the type of the character
    def Start(self):
        self.playing = 1
        mod = 0
        print('Choose the level')
        print('''
                 Easy
                 Medium
                 Hard
                 Extreme ''')
        notValid = True
        while(notValid):
            level = input().lower()
            if level == 'Easy'.lower():
                notValid = False
            elif level == 'Medium'.lower():
                notValid = False
                mod = 1
            elif level == 'Hard'.lower():
                notValid = False
                mod = 2
            elif level == 'Extreme'.lower():
                notValid = False
                mod = 3
            else:
                print('invalid level')
        notValid2 = True
        print('Choose the character between the list')
        print(Character.get_character_list())
        while(notValid2):
            user_name = input().lower()
            for poss_char in Character.get_character_list():
                if user_name == poss_char.lower():
                    notValid2 = False
            if notValid2:
                print('invalid type')
        return [Character.init_given_character(user_name),mod]
    
    #Game: initialization, creation of the enemy according to the level, loop in the 3 possible states while playing
    def play(self):
        start = self.Start()
        player_character = start[0]
        level = start[1]
        print('LOADING...')
        name = player_character.get_type()
        enemy_table_df = pd.read_csv((name + '_enemy_table.csv'),index_col = 0)
        enemy_table_stats_df = pd.read_csv((name + '_enemy_table_stats.csv'),index_col = 0) 
        novice_e_df = pd.read_csv((name + '_novice_e.csv'),index_col = 0)
        skilled_e_df = pd.read_csv((name + '_skilled_e.csv'),index_col = 0)
        size = novice_e_df.shape[0]
        novice_enemy = []
        for i in range(0,size):
            novice_enemy.append(Character('c','NPC',novice_e_df['HPs'][i],novice_e_df['Mps'][i],novice_e_df['Aps'][i],eval(novice_e_df['Action'][i])))
        skilled_enemy = []
        size2 = skilled_e_df.shape[0]
        for i in range(0,size2):
            skilled_enemy.append(Character('d','NPC',skilled_e_df['HPs'][i],skilled_e_df['Mps'][i],skilled_e_df['Aps'][i],eval(skilled_e_df['Action'][i])))
        player_character.rest()
        while(self.playing):
            self.states_menu(player_character,level,novice_enemy,skilled_enemy)

    
    #possible states: quit, explore, rest and back home
    def states_menu(self,character,level,novice_enemy,skilled_enemy):
        print('You are in the location: %4s' % (self.places[self.current_place]))
        print('What do you want to do?')
        print('''
                 Quit
                 Explore
                 Rest and back Home''')
        notValid= True
        while(notValid):
            input_state = input().lower()
            if input_state == 'Quit'.lower():
                self.quit()
                notValid = False
            elif input_state == 'Explore'.lower():
                if(level==0):
                    casual = random.randrange(0,len(novice_enemy))
                    enemy = novice_enemy[casual]
                elif(level==1):
                    casual2 = random.randrange(0,2)
                    if(casual2==0):
                        casual = random.randrange(0,len(novice_enemy))
                        enemy = novice_enemy[casual]
                    else:
                        casual = random.randrange(0,len(skilled_enemy))
                        enemy = skilled_enemy[casual]
                elif(level==2):
                    casual = random.randrange(0,len(skilled_enemy))
                    enemy = skilled_enemy[casual]
                else:
                    enemy = Character.init_random_character()
                self.explore_world(character,enemy,level)
                notValid = False
            elif input_state == 'Rest and back Home'.lower():
                self.rest_BackHome(character)
                notValid = False
            else:
                print('Invalid action')            
            
    #Explore the world: it allows to find an enemy. If the battle begins the player will go to the next place
    #otherwise it does nothing
    def explore_world(self,character,enemy,level):
        print('To move to the next place you have to defeat the enemy, do you want to battle him?')
        print('''
                 Yes
                 NO''')
        notValid = True
        while(notValid):
            answer = input().lower()
            if(answer=='Yes'.lower()):
                if self.current_place == len(self.places)-1:
                    self.final_battle(character,level)                                                   
                else:
                    self.battle(character,enemy)
                    enemy.rest()
                    if character.loose == 0:
                        self.current_place += 1
                    else:
                        character.rest()
                notValid = False
            elif(answer == 'NO'.lower()):
                break
            else:
                print('Invalid action')
             
    #the player stop playing
    def quit(self):
        print('Thank you for playing, see you later')
        self.playing = 0
    
    #the player refill his hp, his mp and come back home without loosing his level and his experience
    def rest_BackHome(self,character):
        character.rest()
        self.current_place = 0
        
    #battle against an enemy: it last until one player will die or the player will escape
    #if the player win, he gains experience and he will move to the next place, if he reaches the necessary exp points
    # he will increment his level incrementing his hp,mp ap
    #else if the player loose he will loose all his exp and his level will be set 0 and he will restart from home
    #Finally if the player escape he will go to the next place but without gaining any experience
    def battle(self,character,random_enemy):
        while((not(random_enemy.is_dead())) and (not(character.is_dead()))):
            print('Your\'s enemy characteristic: Hp:%d, Ap:%s' % (random_enemy.hp, random_enemy.ap))
            print('Player\'s stats: Hp:%d, Ap:%s' % (character.hp,character.ap))                 
            initial_move = random.randrange(0,2)
            if(initial_move == 0):
                #The player start this turn
                print('Choose the number of a move')
                for i in range(0,len(character.actions)):
                    print('Nr:%d, move:%s' % (i, character.actions[i]))                    
                validMove = False
                while(not(validMove)):
                    isInt = False
                    while(not(isInt)):
                        try:
                            nr_move = int(input())
                            isInt = True
                        except ValueError:
                            print('insert the number corresponding to the action, not a letter')
                            isInt = False
                    while(nr_move<0 or nr_move>len(character.actions)):
                        print('Error, wrong move. Choose another move')
                        isInt = False
                        while(not(isInt)):
                            try:
                                nr_move = int(input())
                                isInt = True
                            except ValueError:
                                print('insert the number corresponding to the action, not a letter')
                                isInt = False
                    move_attack = int(character.actions[nr_move]["dmg"])
                    move_prob = character.actions[nr_move]["succ"]
                    move_cost = character.actions[nr_move]["mp_cost"]
                    if(character.mp>move_cost):
                        validMove = True
                    else:
                        print('Not enough magic points')
                success = random.random() < move_prob
                if success:
                    if(nr_move == (len(character.actions)-1)):
                        print('You was able to escape')
                        break
                    elif(nr_move == (len(character.actions)-2)):
                        casual = random.randrange(0,15)
                        casual = casual *0.01
                        if character.hp + character.hp*casual> character.max_hp:
                            character.hp = character.max_hp
                        else:
                            character.hp += character.hp*casual
                            print('Your health was healed of %4d:' % (character.hp*casual))
                    else:
                        print('Your attacck hits the enemy')
                        current_damage = random.randrange(character.ap-5,character.ap+5)
                        total_damage = current_damage*move_attack//5
                        random_enemy.hp = random_enemy.hp-total_damage
                        print('Enemy\'s life: %4s' % (random_enemy.hp))
                        if random_enemy.is_dead():
                            print('Victory')
                            character.exp += 50//(character.player_level+1)
                            if character.exp > 100:
                                character.player_level +=1
                                character.exp = character.exp-100
                                character.increase_Level()
                                print('Your level increase')
                            break
                else:
                    if(nr_move == (len(character.actions)-1)):
                        print('You was not able to escape')
                    else:
                        print('Your attacck does not hit the enemy')
                character.mp = character.mp-move_cost
                # the enemy attack
                Nr_enemy_selectAttack = random.randrange(0,len(random_enemy.actions))
                enemy_selectAttack = random_enemy.actions[Nr_enemy_selectAttack]
                move_attack = int(enemy_selectAttack["dmg"])
                move_prob = enemy_selectAttack["succ"]
                success = random.random() < move_prob
                if success:
                    print('Your enemy\'s attack hits you')
                    current_damage = random.randrange(random_enemy.ap-5,random_enemy.ap+5)
                    total_damage = current_damage*move_attack//5
                    character.hp = character.hp-total_damage
                    print('Player\'s life: %4s' % (character.hp))
                    if character.is_dead():
                        print('You was Killed')
                        print('you loose all you experience and your level is again 0')
                        print('you will restart from home')
                        self.current_place = 0
                        character.exp = 0
                        character.max_hp = character.max_hp - 50*character.player_level
                        character.max_mp = character.max_mp - 5*character.player_level
                        character.ap = character.ap-character.player_level
                        character.player_level = 0
                        character.loose = 1
                        break
                else:
                    print('Your enemy\'s attack don\'t hit you')
            #the enemy starts this turn
            else:   
                # the enemy attack
                Nr_enemy_selectAttack = random.randrange(0,len(random_enemy.actions))
                enemy_selectAttack = random_enemy.actions[Nr_enemy_selectAttack]
                move_attack = int(enemy_selectAttack["dmg"])
                move_prob = enemy_selectAttack["succ"]
                success = random.random() < move_prob
                if success:
                    print('Your enemy\'s attack hits you')
                    current_damage = random.randrange(random_enemy.ap-5,random_enemy.ap+5)
                    total_damage = current_damage*move_attack//5
                    character.hp = character.hp-total_damage
                    print('Player\'s life: %4s' % (character.hp))
                    if character.is_dead():
                        print('You was Killed')
                        print('you loose all you experience and your level is again 0')
                        print('you will restart from home')
                        self.current_place = 0
                        character.exp = 0
                        character.max_hp = character.max_hp - 50*character.player_level
                        character.max_mp = character.max_mp - 5*character.player_level
                        character.ap = character.ap-character.player_level
                        character.player_level = 0
                        character.loose = 1
                        break
                else:
                    print('Your enemy\'s attack don\'t hit you')
                # the player attack
                print('Choose the number of a move')
                for i in range(0,len(character.actions)):
                    print('Nr:%d, move:%s' % (i, character.actions[i]))                    
                validMove = False
                while(not(validMove)):
                    isInt = False
                    while(not(isInt)):
                        try:
                            nr_move = int(input())
                            isInt = True
                        except ValueError:
                            print('insert the number corresponding to the action, not a letter')
                            isInt = False
                    while(nr_move<0 or nr_move>len(character.actions)):
                        print('Error, wrong move. Choose another move')
                        isInt = False
                        while(not(isInt)):
                            try:
                                nr_move = int(input())
                                isInt = True
                            except ValueError:
                                print('insert the number corresponding to the action, not a letter')
                                isInt = False
                    move_attack = int(character.actions[nr_move]["dmg"])
                    move_prob = character.actions[nr_move]["succ"]
                    move_cost = character.actions[nr_move]["mp_cost"]
                    if(character.mp>move_cost):
                        validMove = True
                    else:
                        print('Not enough magic points')
                success = random.random() < move_prob
                if success:
                    if(nr_move == (len(character.actions)-1)):
                        print('You was able to escape')
                        break                    
                    elif(nr_move == (len(character.actions)-2)):
                        casual = random.randrange(0,15)
                        casual = casual *0.01
                        if character.hp + character.hp*casual> character.max_hp:
                            character.hp = character.max_hp
                        else:
                            character.hp += character.hp*casual
                            print('Your health was healed of %4d:' % (character.hp*casual))
                    else:
                        print('Your attacck hits the enemy')
                        current_damage = random.randrange(character.ap-5,character.ap+5)
                        total_damage = current_damage*move_attack//5
                        random_enemy.hp = random_enemy.hp-total_damage
                        print('Enemy\'s life: %4s' % (random_enemy.hp))
                        if random_enemy.is_dead():
                            print('Victory')
                            character.exp += 50//(character.player_level+1)
                            if character.exp > 100:
                                character.player_level +=1
                                character.exp = character.exp-100
                                character.increase_Level()
                                print('Your level increase')
                            break
                else:
                    if(nr_move == (len(character.actions)-1)):
                        print('You was not able to escape')
                    else:
                        print('Your attacck does not hit the enemy')
                character.mp = character.mp-move_cost
                
    #The final battle is against a boss which is a more strong NPC            
    def final_battle(self,character,level):
        print('This is the FINAL BATTLE, you will be heal and you will fight against the FINAL BOSS')
        if level == 0:
            boss = self.FindSkiNPCGivenT(character)
        elif level == 1:
            boss = Character.init_random_character()
        elif level == 2:
            boss = character
        else:
            boss = Character('Final BOSS','NPC',character.hp+200,character.mp+50,character.ap+10,character.actions)
        character.rest()
        self.battle(character,boss)
        if(character.loose==0):
            print('You win')
            print('''
                                 |--|   |--|      |--||--|    |--|    |--|     
                                 |- -| |- -|    |--|    |--|  |--|    |--|
                                  |- -|- -|    |--|      |--| |--|    |--|
                                    |- -|       |--|    |--|  |--|----|--|
                                    |- -|         |--||--|    |----------|      ''')
            print('''
                                 |--|   |--|   |--|   |--|    |-|     |-|     
                                 |- -| |-  -| |- -|   |--|    |-||-|  |-|
                                  |- -|      |- -|    |--|    |-|  |-||-|
                                   |-|        |-|     |--|    |-|    |--| ''')
            self.playing = 0
        else:
            character.rest()
              
    def random_battle_generator(self,character1,character2,nr_turn):
        victory = 0
        mean_turn = 0
        mean_hp_P = 0
        mean_hp_E = 0
        mean_mp_P = 0
        mean_mp_E = 0
        j = 0
        turn_stats = []
        while j<nr_turn:
            turn = 0
            character1.rest()
            character2.rest()
            while((not(character1.is_dead())) and (not(character2.is_dead()))):              
                initial_move = random.randrange(0,2)
                turn += 1
                if(initial_move == 0):
                    #The player1 start this turn
                    validMove = False
                    while not(validMove):
                        Nr_character1_selectAttack = random.randrange(0,len(character1.actions))
                        character1_selectAttack = character1.actions[Nr_character1_selectAttack] 
                        character1_move_attack = character1.actions[Nr_character1_selectAttack]["dmg"]
                        character1_move_prob = character1.actions[Nr_character1_selectAttack]["succ"]
                        character1_move_cost = character1.actions[Nr_character1_selectAttack]["mp_cost"]
                        if(character1.mp>character1_move_cost):
                            validMove = True
                    success = random.random() < character1_move_prob
                    previousE_h = character2.hp
                    if success:
                        character1_current_damage = random.randrange(character1.ap-5,character1.ap+5)
                        character1_total_damage = character1_current_damage*character1_move_attack//5
                        character2.hp = character2.hp-character1_total_damage
                        if character2.is_dead():
                            victory = victory + 1
                            mean_hp_P += character1.hp
                            mean_mp_P += character1.mp
                            mean_mp_E += character2.mp
                            turn_stats.append({'Attacker' : 'Player', 'Action' : character1_selectAttack, 'Dmg_done': previousE_h-character2.hp, 'Dmg_receive': 0,'HealthP': character1.hp, 'MPPlayer':character1.mp,'HealthE': character2.hp,'MPE':character2.hp})
                            break
                    character1.mp = character1.mp-character1_move_cost
                    turn_stats.append({'Attacker' : 'Player', 'Action' : character1_selectAttack, 'Dmg_done': previousE_h-character2.hp, 'Dmg_receive': 0,'HealthP': character1.hp, 'MPPlayer':character1.mp,'HealthE': character2.hp,'MPE':character2.hp})
                    # the character2 attack
                    validMove = False
                    while not(validMove):
                        Nr_character2_selectAttack = random.randrange(0,len(character2.actions))
                        character2_selectAttack = character2.actions[Nr_character2_selectAttack] 
                        move_attack = character2.actions[Nr_character2_selectAttack]["dmg"]
                        move_prob = character2.actions[Nr_character2_selectAttack]["succ"]
                        move_cost = character2.actions[Nr_character2_selectAttack]["mp_cost"]
                        if(character2.mp>move_cost):
                            validMove = True
                    previousP_h = character1.hp
                    success = random.random() < move_prob
                    if success:
                        current_damage = random.randrange(character2.ap-5,character2.ap+5)
                        total_damage = current_damage*move_attack//5
                        character1.hp = character1.hp-total_damage
                        if character1.is_dead():
                            mean_hp_E += character2.hp
                            mean_mp_P += character1.mp
                            mean_mp_E += character2.mp
                            turn_stats.append({'Attacker' : 'NPC', 'Action' : character2_selectAttack, 'Dmg_done': 0, 'Dmg_receive': previousP_h-character1.hp,'HealthP': character1.hp, 'MPPlayer':character1.mp,'HealthE': character2.hp,'MPE':character2.hp})
                            break
                    character2.mp = character2.mp-move_cost
                    turn_stats.append({'Attacker' : 'NPC', 'Action' : character2_selectAttack, 'Dmg_done': 0, 'Dmg_receive': previousP_h-character1.hp,'HealthP': character1.hp, 'MPPlayer':character1.mp,'HealthE': character2.hp,'MPE':character2.hp})
                #the character2 starts this turn
                else:   
                    # the character2 attack
                    validMove = False
                    while not(validMove):
                        Nr_character2_selectAttack = random.randrange(0,len(character2.actions))
                        character2_selectAttack = character2.actions[Nr_character2_selectAttack] 
                        move_attack = character2.actions[Nr_character2_selectAttack]["dmg"]
                        move_prob = character2.actions[Nr_character2_selectAttack]["succ"]
                        move_cost = character2.actions[Nr_character2_selectAttack]["mp_cost"]
                        if(character2.mp>move_cost):
                            validMove = True
                    previousP_h = character1.hp
                    success = random.random() < move_prob
                    if success:
                        current_damage = random.randrange(character2.ap-5,character2.ap+5)
                        total_damage = current_damage*move_attack//5
                        character1.hp = character1.hp-total_damage
                        if character1.is_dead():
                            mean_hp_E += character2.hp
                            mean_mp_P += character1.mp
                            mean_mp_E += character2.mp
                            turn_stats.append({'Attacker' : 'NPC', 'Action' : character2_selectAttack, 'Dmg_done': 0, 'Dmg_receive': previousP_h-character1.hp,'HealthP': character1.hp, 'MPPlayer':character1.mp,'HealthE': character2.hp,'MPE':character2.hp})
                            break
                    turn_stats.append({'Attacker' : 'NPC', 'Action' : character2_selectAttack, 'Dmg_done': 0, 'Dmg_receive': previousP_h-character1.hp,'HealthP': character1.hp, 'MPPlayer':character1.mp,'HealthE': character2.hp,'MPE':character2.hp})
                    character2.mp = character2.mp-move_cost
                    # the character1 attack
                    validMove = False
                    while not(validMove):
                        Nr_character1_selectAttack = random.randrange(0,len(character1.actions))
                        character1_selectAttack = character1.actions[Nr_character1_selectAttack] 
                        character1_move_attack = character1.actions[Nr_character1_selectAttack]["dmg"]
                        character1_move_prob = character1.actions[Nr_character1_selectAttack]["succ"]
                        character1_move_cost = character1.actions[Nr_character1_selectAttack]["mp_cost"]
                        if(character1.mp>character1_move_cost):
                            validMove = True
                    previousE_h = character2.hp
                    success = random.random() < character1_move_prob
                    if success:
                        character1_current_damage = random.randrange(character1.ap-5,character1.ap+5)
                        character1_total_damage = character1_current_damage*character1_move_attack//5
                        character2.hp = character2.hp-character1_total_damage
                        if character2.is_dead():
                            victory = victory + 1
                            mean_hp_P += character1.hp
                            mean_mp_P += character1.mp
                            mean_mp_E += character2.mp
                            turn_stats.append({'Attacker' : 'Player', 'Action' : character1_selectAttack, 'Dmg_done': previousE_h-character2.hp, 'Dmg_receive': 0,'HealthP': character1.hp, 'MPPlayer':character1.mp,'HealthE': character2.hp,'MPE':character2.hp})
                            break
                    character1.mp = character1.mp-character1_move_cost
                    turn_stats.append({'Attacker' : 'Player', 'Action' : character1_selectAttack, 'Dmg_done': previousE_h-character2.hp, 'Dmg_receive': 0,'HealthP': character1.hp, 'MPPlayer':character1.mp,'HealthE': character2.hp,'MPE':character2.hp})
            mean_turn += turn
            j = j+1    
        mean_turn = mean_turn/nr_turn    
        victory = victory/nr_turn
        loss = 1-victory
        mean_hp_P = mean_hp_P/nr_turn
        mean_hp_E = mean_hp_E/nr_turn
        mean_mp_P = mean_mp_P/nr_turn
        mean_mp_E = mean_mp_E/nr_turn
        return [victory, loss, mean_turn ,mean_hp_P,mean_hp_E,mean_mp_P,mean_mp_E,turn_stats]
    
    #return the variance over the perc of the victory of 100 battles of n simulation 
    def calc_var(self,character1,character2,n,firstM):
        var = 0
        for i in range(0,n-1):
            this_turnStat = self.random_battle_generator(character1,character2,100)
            this_turnV = this_turnStat[0]
            var += (this_turnV-firstM)**2
        var = var/n
        return var            
            
    
    def FindNovAndSkiNPC(self,player_character_toS,enemy_table):
        stats = []
        novice_user_NPC = []
        skilled_user_NPC = []
        novelNPC_enemy= []
        skilledNPC_enemy= []
        for i in range(0,len(enemy_table)-1):
            stats.append(self.random_battle_generator(player_character_toS,Character('a','NPC', int(enemy_table[i][0]), int(enemy_table[i][1]), int(enemy_table[i][2]), enemy_table[i][3]),100))
            player_character_toS.rest()
            novice_user = True
            skilled_user = True
            if stats[i][0]<0.96:
                novice_user = False
            if stats[i][3]<0.7*player_character_toS.hp:
                novice_user = False
            if stats[i][5]<0.5*player_character_toS.mp:
                novice_user = False
            if novice_user:
                novice_user_NPC.append(i)
                novelNPC_enemy.append([i,enemy_table[i][0],enemy_table[i][1],int(enemy_table[i][2]),enemy_table[i][3]])
            else:
                if (stats[i][0]<0.57) or (stats[i][0]>0.63):
                    skilled_user = False
                if stats[i][2]<4:
                    skilled_user = False
                if skilled_user:
                    skilled_user_NPC.append(i)
                    skilledNPC_enemy.append([i,enemy_table[i][0],enemy_table[i][1],int(enemy_table[i][2]),enemy_table[i][3]])
        return [novelNPC_enemy, skilledNPC_enemy]

            

    def FindNovNPC(self,player_character_toS,enemy_table):
        stats = []
        novice_user_NPC = []
        for i in range(0,len(enemy_table)-1):
            stats.append(self.random_battle_generator(player_character_toS,Character('a','NPC', int(enemy_table[i][0]), int(enemy_table[i][1]), int(enemy_table[i][2]), enemy_table[i][3]),100))
            player_character_toS.rest()
            novice_user = True
            if stats[i][0]<0.96:
                novice_user = False
            if stats[i][3]<0.7*player_character_toS.hp:
                novice_user = False
            if stats[i][5]<0.5*player_character_toS.mp:
                novice_user = False
            if novice_user:
                novice_user_NPC.append(i)
        min_var_victory = 100
        index_min_var = 0
        #print('Novice user NPC')
        for j in range(0,len(novice_user_NPC)-1):
            #print(novice_user_NPC[j])
            #var_j = abs(stats[novice_user_NPC[j]][0]-0.98)
            var_j = self.calc_var(player_character_toS,Character('a','NPC', int(enemy_table[novice_user_NPC[j]][0]), int(enemy_table[novice_user_NPC[j]][1]), int(enemy_table[novice_user_NPC[j]][2]), enemy_table[novice_user_NPC[j]][3]),10,0.98)
            if var_j<min_var_victory:
                min_var_victory = var_j
                index_min_var = j

        novelNPC_enemy = Character('novel','NPC',enemy_table[novice_user_NPC[index_min_var]][0],enemy_table[novice_user_NPC[index_min_var]][1],int(enemy_table[novice_user_NPC[index_min_var]][2]),enemy_table[novice_user_NPC[index_min_var]][3])
        return novelNPC_enemy
    
    def FindSkiNPC(self,player_character_toS,enemy_table,var):
        stats = []
        skilled_user_NPC = []
        for i in range(0,len(enemy_table)-1):
            stats.append(self.random_battle_generator(player_character_toS,Character('a','NPC', int(enemy_table[i][0]), int(enemy_table[i][1]), int(enemy_table[i][2]), enemy_table[i][3]),100))
            player_character_toS.rest()
            skilled_user = True
            if (stats[i][0]<0.6-var) or (stats[i][0]>0.6+var):
                skilled_user = False
            if stats[i][2]<7:
                skilled_user = False
            if skilled_user:
                skilled_user_NPC.append(i)
        min_var_victory_skilled = 100
        index_min_var_skilled = 0
        #print('Skilled user NPC')
        for m in range(0,len(skilled_user_NPC)-1):
            #print(skilled_user_NPC[m])
            #var_m = abs(stats[skilled_user_NPC[m]][0]-0.6)
            var_m = self.calc_var(player_character_toS,Character('a','NPC', int(enemy_table[skilled_user_NPC[m]][0]), int(enemy_table[skilled_user_NPC[m]][1]), int(enemy_table[skilled_user_NPC[m]][2]), enemy_table[skilled_user_NPC[m]][3]),10,0.6)
            if var_m<min_var_victory_skilled:
                min_var_victory_skilled = var_m
                index_min_var_skilled = m
        if len(skilled_user_NPC) == 0:
            skilledNPC_enemy = self.FindSkiNPC(player_character_toS,enemy_table,var+0.03)
        else:
            skilledNPC_enemy = Character('skilled','NPC',enemy_table[skilled_user_NPC[index_min_var_skilled]][0],enemy_table[skilled_user_NPC[index_min_var_skilled]][1],int(enemy_table[skilled_user_NPC[index_min_var_skilled]][2]),enemy_table[skilled_user_NPC[index_min_var_skilled]][3])
        return skilledNPC_enemy
    
    def FindNovNPCGivenT(self,player_character_toS):
        actions_set1 =     [{'name': 'Punch', 'type': 'physical', 'dmg': 50, 'succ': 0.90,'mp_cost': 0},
                           {'name': 'Kick', 'type': 'physical', 'dmg': 60, 'succ': 0.80, 'mp_cost': 0},
                           {'name': 'Sword cleave', 'type': 'physical', 'dmg': 80, 'succ': 0.80, 'mp_cost': 0},
                           {'name': 'Magic sword cleave', 'type': 'physical', 'dmg': 100, 'succ': 0.55, 'mp_cost': 10}]

        actions_set2 =     [{'name': 'Punch', 'type': 'physical', 'dmg': 50, 'succ': 0.90, 'mp_cost': 0},
                           {'name': 'Kick', 'type': 'physical', 'dmg': 60, 'succ': 0.80, 'mp_cost': 0},
                           {'name': 'Fire', 'type': 'spell', 'dmg': 75, 'succ': 0.80, 'mp_cost': 10},
                           {'name': 'Thunder', 'type': 'spell', 'dmg': 110, 'succ': 0.5, 'mp_cost': 20},
                           {'name': 'Blizzard', 'type': 'spell', 'dmg': 85, 'succ': 0.65, 'mp_cost': 10}]

        actions_set3 =     [{'name': 'Punch', 'type': 'physical', 'dmg': 50, 'succ': 0.90, 'mp_cost': 0},
                           {'name': 'Kick', 'type': 'physical', 'dmg': 60, 'succ': 0.80, 'mp_cost': 0},
                           {'name': 'Shiv', 'type': 'physical', 'dmg': 80, 'succ': 0.80, 'mp_cost': 0},
                           {'name': 'Magic Shiv', 'type': 'spell', 'dmg': 100, 'succ': 0.75, 'mp_cost': 10}]

        actions_set4 =     [{'name': 'Punch', 'type': 'physical', 'dmg': 50, 'succ': 0.90,'mp_cost': 0},
                            {'name': 'Kick', 'type': 'physical', 'dmg': 60, 'succ': 0.80, 'mp_cost': 0},
                            {'name': 'Skyuppercut', 'type': 'physical', 'dmg': 90, 'succ': 0.75, 'mp_cost': 0},
                            {'name': 'Chuck Norris roundhouse kick', 'type': 'physical', 'dmg': 9999, 'succ': 0.01, 'mp_cost': 0}]

        ranges = {'HPs' : np.linspace(20,2000,6),
                'Mps' : np.linspace(10,100,6),
                  'Aps' : np.linspace(1,50,6),
                  'Action' : [actions_set1,actions_set2,actions_set3,actions_set4]}

        enemy_table = np.array(np.meshgrid(ranges['HPs'],ranges['Mps'],ranges['Aps'],ranges['Action']))
        enemy_table = enemy_table.T.reshape(-1,4)
        return self.FindNovNPC(player_character_toS,enemy_table)
    
    def FindSkiNPCGivenT(self,player_character_toS):
        actions_set1 =     [{'name': 'Punch', 'type': 'physical', 'dmg': 50, 'succ': 0.90,'mp_cost': 0},
                           {'name': 'Kick', 'type': 'physical', 'dmg': 60, 'succ': 0.80, 'mp_cost': 0},
                           {'name': 'Sword cleave', 'type': 'physical', 'dmg': 80, 'succ': 0.80, 'mp_cost': 0},
                           {'name': 'Magic sword cleave', 'type': 'physical', 'dmg': 100, 'succ': 0.55, 'mp_cost': 10}]

        actions_set2 =     [{'name': 'Punch', 'type': 'physical', 'dmg': 50, 'succ': 0.90, 'mp_cost': 0},
                           {'name': 'Kick', 'type': 'physical', 'dmg': 60, 'succ': 0.80, 'mp_cost': 0},
                           {'name': 'Fire', 'type': 'spell', 'dmg': 75, 'succ': 0.80, 'mp_cost': 10},
                           {'name': 'Thunder', 'type': 'spell', 'dmg': 110, 'succ': 0.5, 'mp_cost': 20},
                           {'name': 'Blizzard', 'type': 'spell', 'dmg': 85, 'succ': 0.65, 'mp_cost': 10}]

        actions_set3 =     [{'name': 'Punch', 'type': 'physical', 'dmg': 50, 'succ': 0.90, 'mp_cost': 0},
                           {'name': 'Kick', 'type': 'physical', 'dmg': 60, 'succ': 0.80, 'mp_cost': 0},
                           {'name': 'Shiv', 'type': 'physical', 'dmg': 80, 'succ': 0.80, 'mp_cost': 0},
                           {'name': 'Magic Shiv', 'type': 'spell', 'dmg': 100, 'succ': 0.75, 'mp_cost': 10}]

        actions_set4 =     [{'name': 'Punch', 'type': 'physical', 'dmg': 50, 'succ': 0.90,'mp_cost': 0},
                            {'name': 'Kick', 'type': 'physical', 'dmg': 60, 'succ': 0.80, 'mp_cost': 0},
                            {'name': 'Skyuppercut', 'type': 'physical', 'dmg': 90, 'succ': 0.75, 'mp_cost': 0},
                            {'name': 'Chuck Norris roundhouse kick', 'type': 'physical', 'dmg': 9999, 'succ': 0.01, 'mp_cost': 0}]

        ranges = {'HPs' : np.linspace(20,2000,6),
                  'Mps' : np.linspace(10,100,6),
                  'Aps' : np.linspace(1,50,6),
                  'Action' : [actions_set1,actions_set2,actions_set3,actions_set4]}

        enemy_table = np.array(np.meshgrid(ranges['HPs'],ranges['Mps'],ranges['Aps'],ranges['Action']))
        enemy_table = enemy_table.T.reshape(-1,4)
        enemy_table_df = pd.DataFrame(enemy_table, columns=ranges.keys())
        enemy_table_df.to_csv('enemy_table.csv')
        return self.FindSkiNPC(player_character_toS,enemy_table,0.03)
    
    def FindSkiandNovNPCGivenT(self,player_character_toS):
        actions_set1 =     [{'name': 'Punch', 'type': 'physical', 'dmg': 50, 'succ': 0.90,'mp_cost': 0},
                           {'name': 'Kick', 'type': 'physical', 'dmg': 60, 'succ': 0.80, 'mp_cost': 0},
                           {'name': 'Sword cleave', 'type': 'physical', 'dmg': 80, 'succ': 0.80, 'mp_cost': 0},
                           {'name': 'Magic sword cleave', 'type': 'physical', 'dmg': 100, 'succ': 0.55, 'mp_cost': 10}]

        actions_set2 =     [{'name': 'Punch', 'type': 'physical', 'dmg': 50, 'succ': 0.90, 'mp_cost': 0},
                           {'name': 'Kick', 'type': 'physical', 'dmg': 60, 'succ': 0.80, 'mp_cost': 0},
                           {'name': 'Fire', 'type': 'spell', 'dmg': 75, 'succ': 0.80, 'mp_cost': 10},
                           {'name': 'Thunder', 'type': 'spell', 'dmg': 110, 'succ': 0.5, 'mp_cost': 20},
                           {'name': 'Blizzard', 'type': 'spell', 'dmg': 85, 'succ': 0.65, 'mp_cost': 10}]

        actions_set3 =     [{'name': 'Punch', 'type': 'physical', 'dmg': 50, 'succ': 0.90, 'mp_cost': 0},
                           {'name': 'Kick', 'type': 'physical', 'dmg': 60, 'succ': 0.80, 'mp_cost': 0},
                           {'name': 'Shiv', 'type': 'physical', 'dmg': 80, 'succ': 0.80, 'mp_cost': 0},
                           {'name': 'Magic Shiv', 'type': 'spell', 'dmg': 100, 'succ': 0.75, 'mp_cost': 10}]

        actions_set4 =     [{'name': 'Punch', 'type': 'physical', 'dmg': 50, 'succ': 0.90,'mp_cost': 0},
                            {'name': 'Kick', 'type': 'physical', 'dmg': 60, 'succ': 0.80, 'mp_cost': 0},
                            {'name': 'Skyuppercut', 'type': 'physical', 'dmg': 90, 'succ': 0.75, 'mp_cost': 0},
                            {'name': 'Chuck Norris roundhouse kick', 'type': 'physical', 'dmg': 9999, 'succ': 0.01, 'mp_cost': 0}]

        ranges = {'HPs' : np.linspace(20,2000,6),
                  'Mps' : np.linspace(10,100,6),
                  'Aps' : np.linspace(1,50,6),
                  'Action' : [actions_set1,actions_set2,actions_set3,actions_set4]}
        ranges_skiNo = {'Position':0,'HPs' : 0, 'Mps' : 0,'Aps' : 0, 'Action' : 0}
        ranges_battle = {'Victory': 0, 'Loss':0, 'Mean turn' :0, 'Mean Hp Player': 0, 'Mean Hp enemy': 0, 'Mean Mp player': 0, 'Mean Mp enemy': 0, 'Turns Stats':0}
        enemy_table = np.array(np.meshgrid(ranges['HPs'],ranges['Mps'],ranges['Aps'],ranges['Action']))
        enemy_table = enemy_table.T.reshape(-1,4)
        enemy_table_df = pd.DataFrame(enemy_table, columns=ranges.keys())
        enemy_table_df.to_csv('Wizard_enemy_table.csv')
        stats = []
        for i in range (len(enemy_table)):
            enemy_i = Character('b','NPC',enemy_table[i][0],enemy_table[i][1],int(enemy_table[i][2]),enemy_table[i][3])
            stats.append(self.random_battle_generator(player_character_toS,enemy_i,100))
        stats_df = pd.DataFrame(stats,columns = ranges_battle.keys())
        stats_df.to_csv('Wizard_enemy_table_stats.csv')
        [novice_e, skilled_e] = self.FindNovAndSkiNPC(player_character_toS,enemy_table)
        novice_e_df = pd.DataFrame(novice_e,columns = ranges_skiNo.keys())
        skilled_e_df = pd.DataFrame(skilled_e,columns = ranges_skiNo.keys())
        novice_e_df.to_csv('Wizard_novice_e.csv')
        skilled_e_df.to_csv('Wizard_skilled_e.csv')


# In[27]:


game = GameEngine()
game.play()


# In[30]:


#creation of table
game2 = GameEngine()
awarrior = Wizard()
game2.FindSkiandNovNPCGivenT(awarrior)


# In[ ]:




